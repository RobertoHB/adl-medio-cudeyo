<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AlumnosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Talentos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="talentos-index" style="padding:5px 50px">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Talento', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            //'id ',
            [
                'label'=>'Nº Talento', 
                'attribute'=> 'id',
                 'headerOptions' => ['style' => 'width:50px;']
            ],
           
            [
                'label'=>'Nombre', 
                'attribute'=> 'nombre',
                 'headerOptions' => ['style' => 'width:50px;']
            ],
        
           [
                'label'=>'Apellidos', 
                'attribute'=> 'apellidos',
                 'headerOptions' => ['style' => 'width:120px;']
            ],
           
             [
                'label'=>'Profesión', 
                'attribute'=> 'profesion',
                 'headerOptions' => ['style' => 'width:150px;']
            ],
             [
             'label'=>'Particular',
             'attribute'=>'particular',
             'format'=>'raw',
             'headerOptions' => ['style'=>'text-align:center;width:25px;'],
             'contentOptions' => function ($model) {
                             return ['style' => 'color:' 
                                 .($model->particular === 0 ? 'green' : 'green')];
                            },
             'filter' => [0=>'No',1=>'Si'],  
             'headerOptions' => ['style' => 'width:80px;'],
             
             //'contentOptions' => [0=>['style'=>'color:red'],1=>['style'=>'color:blue']],   
             'value' => function($model) {
                            return $model->particular == 1 ? 'Si' : 'No';}             
            ],
             [
             'label'=>'Empresa',
             'attribute'=>'empresa',
             'format'=>'raw',
             'headerOptions' => ['style'=>'text-align:center;width:25px;'],
             'contentOptions' => function ($model) {
                             return ['style' => 'color:' 
                                 .($model->empresa === 0 ? 'green' : 'green')];
                            },
             'filter' => [0=>'No',1=>'Si'],  
             'headerOptions' => ['style' => 'width:80px;'],
             
             //'contentOptions' => [0=>['style'=>'color:red'],1=>['style'=>'color:blue']],   
             'value' => function($model) {
                            return $model->empresa == 1 ? 'Si' : 'No';}             
            ],
            
         

            [
                'label'=>'Telefono', 
                'attribute'=> 'telefono',
                 'headerOptions' => ['style' => 'width:120px;']
            ],
            [
                'label'=>'Email', 
                'attribute'=> 'email',
                 'headerOptions' => ['style' => 'width:120px;']
            ],
             [
                'label'=>'Web', 
                'attribute'=> 'web',
                 'headerOptions' => ['style' => 'width:120px;']
            ],
              [
                'label'=>'Linkedin', 
                'attribute'=> 'linkedin',
                 'headerOptions' => ['style' => 'width:120px;']
            ],
          
          

            ['class' => 'yii\grid\ActionColumn',
                 'contentOptions'=>['style'=>'width: 70px;'],
                
            ],
      
        ],
    ]); ?>


</div>
