<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\User;



AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <style>
        .titulo{
            font-size: 2.5em;
        }
    </style>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
  NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'options' => [
      'class' => 'titulo',
    ],
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
      'class' => 'navbar-inverse navbar-fixed-top',
    ],
  ]);
  
  
     if(Yii::$app->user->isGuest){
     
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                   
                     
                    ['label' => 'Entrar', 'url' => ['/site/login']],
                    ['label' => 'Regístrate', 'url' => ['/site/preregistro']],
         
                ],
           ]);
      
         
	}

    if(!Yii::$app->user->isGuest && User::isUserAdmin(Yii::$app->user->identity->getId())){


      echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [

          	['label' => 'Talentos', 'url' => ['/talentos']],
          	['label' => 'Registro', 'url' => ['/site/register']],

          	'<li>'
          		. Html::beginForm(['/site/logout'], 'post')
          		. Html::submitButton(
            		'Logout (' . Yii::$app->user->identity->username . ')',
            			['class' => 'btn btn-link logout']
          		)
          		. Html::endForm()
          	.'</li>'
        ],
      ]);
        
  }

    if(!Yii::$app->user->isGuest && User::isUserUsuario(Yii::$app->user->identity->getId())){

      echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [

            ['label' =>  Yii::$app->user->identity->username ,'url' => ['#'],
               'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
               'items' => [
                             ['label' => 'Talentos', 'url' => ['/site/index'],'linkOptions' => ['target'=>'_blank']],
                             ['label' => 'Mi Perfil', 'url' => ['/talentos/perfil']],
                             ['label' => 'Modificar contraseña', 'url' => ['/site/recoverpass']],
                 			 '<li">'
                               . Html::beginForm(['/site/logout'], 'post')
                               . Html::submitButton(
                                 '<a style="color:#9d9d9d;text-decoration:none;font-size:14px;padding:5px 15px 5px 5px">Cerrar Sesión</a>',
                                 ['class' => 'btn btn-link']
                               )
                               . Html::endForm()
                             . '</li>'           
    
                         ],
            ],
        ],

      ]);
    }     

NavBar::end();  

    ?>

    <div>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="">
    <div class="container">

    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
