<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;
$tipo = ['Particular'=>'Particular','Empresa'=>'Empresa'];
$url = Yii::getAlias("@web") . '/img/';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<title>Listado Perfiles</title>
	<style type="text/css">
                main{
                        
                }
		.ayto{
			padding-top: 10px;
		}
		.logo{
			padding-top: 10px;
		}
		.camino{
			
		}
		.cabecera{
			margin-bottom: 60px;
			padding: 40px 0;
		}
		.pie{
			height: 220px;
			margin-top: 120px;
		}
		.gobierno{
			text-align: right;
			padding-top: 60px;
		}
		.emcan{
			text-align: center;
			padding-top: 90px;
		}
		.cantabria{
			padding-top: 30px;
		}
		.textopie{
			padding: 80px 20px;
		}
		p.enelpie{
			font-weight: bold;
			font-size: 1.2em;
		}
		.fondo{
/*			background-image: '../../web/img/casacorpas3.png';*/
			background-size: cover;
			width: 100%;
			height: 100%;
			position: relative;
		}
		.fondo:before{
			content: "";
			width: 100%;
			height: 100%;
			background-color: #cccccc;
			position: absolute;
			top: 0;
			left: 0;
			opacity: 0.8;
		}

</style>
</head>
<body>

	<div class="col-md-12" style="padding-bottom:140px">
		<div class="camino col-md-4">
			<img src="<?=Url::to('@web/img/lg_camino.png')?>" alt="camino" width="390px" />
		</div>

		<div class="ayto col-md-4">
			<p style="font-weight:bold; font-size:1.3em; text-align:center; padding: 44px 28px 0;">Lanzadera de Empleo y Emprendimiento Solidario del Ayuntamiento de Medio Cudeyo</p>
		</div>

		<div class="ayto col-md-2">
			<img src="<?=Url::to('@web/img/lg_ayuntamiento.png')?>" alt="lanzaderas" width="156px" />
		</div>

		<div class="logo col-md-2 col-12">
                        <a href="https://www.cantabriaemprendedora.es/es/lanzaderas-cantabria-que-son-y-como-participar" target="_blank"><img src="<?=Url::to('@web/img/lg_lanzaderas3.png')?>" alt="lanzaderas" width="120px" /></a>
		</div>
	</div>

	<div class="col-md-12" style="padding:0 60px">

		<div class="col-md-9">
			<p style="font-size: 1.2em; padding:0 20px; text-align:justify;">Este espacio contribuye a una mayor difusión de los perfiles profesionales de aquellas personas que han participado en la <strong>LANZADERA DE EMPLEO Y EMPRENDIMIENTO SOLIDARIO DEL AYUNTAMIENTO DE MEDIO CUDEYO</strong> (en registro, deberán firmar su consentimiento para publicar sus perfiles).
			Es propósito de este portal, facilitar que las empresas encuentren profesionales con talento, así como para que particulares encuentren los servicios que buscan en empresas y/o modelos de negocio, donde se puedan crear sinergias  entre profesionales. Haciendose visibles también se contribuye a una  mejor imagen del proyecto LEES.</p>
		</div>

		<div class="queson col-md-3">
			<button type="button" class="btn" style="background-color:#3A3A3A; width:100%; margin:18px 20px; height:100px;"><a href="<?=Url::to('@web/img/lg_lanzaderas2.png')?>" style="text-decoration:none; color:white; font-size:1.4em">¿Qué son las Lanzaderas?</a></button>
		</div>	
	</div>

	<div class="fondo col-md-12" style="width: 100%; margin-top:40px;background:url(<?= $url ?>casacorpas3.png);background-size:cover">	
		<div class="col-md-12" style="margin-top:20px;">
			<p style="text-align:center; font-weight:bold; font-size:2.6em; font-family:serif; color:#3A3A3A; border-radius:20px; padding:6px 0">¿Estás Buscando Empresas o Talentos?</p>
			<p style="text-align:center; font-size:1.2em; font-weight:bold; padding:8px 0">Accede a toda la información relativa a los perfiles profesionales</p>
		</div>

		<div class="col-md-12" style="display:flex; justify-content:center">
			<div class="col-md-6" style="text-align:center; padding:12px 0; background-color:#EB8448; border-radius:8px; width:30%; margin-right:30px">
				<input type="radio" id="profesionales" name="" value="">
				<label style="font-size:1em" for="profesionales">Encuentra profesionales</label>
			</div>

			<div class="col-md-6" style="text-align:center; padding:12px 0; background-color:#C2C85C; border-radius:8px; width:30%; margin-left:30px">
				<input type="radio" id="empresas" name="" value="">
				<label style="font-size:1em" for="empresas">Encuentra la empresa que necesitas</label>
			</div>
		</div>

                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
		<div class="col-md-12" style="padding: 30px 120px; display:flex; justify-content:center">
                    <div class="col-md-8">
                          <?= $form->field($modelo, 'textoBuscar')->textInput(['maxlength' => true,'placeholder' => 'Introduce el texto a buscar','class'=>'form-control','style'=>'width:100%']) ?>
                    </div>
                    <div class="col-md-4">
                        <?= Html::submitButton('Buscar', ['class' => 'btn btn-dark','style'=>'width:100%;font-weight:bold']) ?>
                    </div>
			   
		</div>
	
		<div class="col-md-12" style="margin-bottom:20px">
			<p style="font-size: 1.2em; padding:0 100px; text-align:justify">Las empresas interesadas en cualquiera de los pefiles profesionales de estas/os profesionales, también puden ponerse en contacto directamente a través de la Agencia de Desarrollo local del Ayuntamiento de Medio Cudeyo.</p>
			<p style="font-size: 1.2em; padding:0 100px; text-align:justify"><strong>Tlf: 942 522 833</strong></p>
			<p style="font-size: 1.2em; padding:0 100px; text-align:justify">Persona de contacto: Isabel Díaz (ADL).</p>
			<p style="font-size: 1.2em; padding:0 100px; text-align:justify">Las personas que han publicado sus perfiles profesionales en esta web que decidan darse de baja en este portal, deberán comunicarlo a la Agencia de Desarrollo Local del Ayuntamiento de Medio Cudeyo en el momento que se efectúe la baja.</p>
		</div>
	</div>
  
    <?php ActiveForm::end(); ?>
    
    
    </div>

     
           <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'summary' => "",
                'itemView' => function ($model, $key, $index, $widget) {
                        return $this->render('_post',['model' => $model]);
                      },
                ]);     
    
    
    ?>
    
    <div class="pie col-md-12">
        <div class="textopie col-md-5">
                <p class="enelpie">Agencia de Desarrollo local del Ayuntamiento de Medio Cudeyo</p>
                <p class="enelpie">Tlf: 942 522 833</p>
        </div>
        <div class="gobierno col-md-3">
            <img src="<?=Url::to('@web/img/lg_gobierno2.png')?>" alt="gobierno" width="160px"/>
        </div>
        <div class="emcan col-md-2">
            <a href="https://www.empleacantabria.es/" target="_blank"><img src="<?=Url::to('@web/img/lg_emcan2.png')?>" alt="emcan" width="200px"/></a>
        </div>
        <div class="cantabria col-md-2">
            <img src="<?=Url::to('@web/img/lg_cantabria2.png')?>" alt="cantabria" width="150px"/>
        </div>
    </div>
    
 

</body>
</html>