
<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\Link;



$Apelnomb = $model->nombre." ".$model->apellidos;
$rutaFoto = '@web/img/'.$model->lanzadera.'/'.$model->apellidos.$model->nombre.$model->id.'.png';
$linkedin = $model->linkedin;
//echo $linkedin;
//exit;

?>
       <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

      
        <div class="col-md-12" style="padding:0 140px;text-align: right;">
		<div class="col-md-4">
			<img src="<?=Url::to($rutaFoto)?>" alt="candidato" height="240px;" width="200px;" style="border-radius: 6%;" />
		</div>
		<div class="col-md-8">
			<h3><?= $Apelnomb ?></h3>
			<h2 style="color: #F2293A; font-family: Brush Script MT; font-weight: bold;"><?= $model->profesion ?></h2>
			<h3>Lanzadera de Medio Cudeyo</h3>
		</div>
	</div>

	<div class="col-md-12" style="padding: 50px 140px;text-align: justify;">
		<h4 style="font-weight: bold;">ALGO SOBRE MI</h4>
		<p style="font-size: 1.4em; font-weight: bold"><?= $model->sobremi?></p>
		<h4 style="font-weight: bold; padding-top: 40px;">MI CARTA DE PRESENTACION</h4>
		<p style="font-size: 1.4em;"><?= $model->carta ?></p>
	</div>
	
	<div class="col-md-12" style="padding: 40px 140px;">
		<h4 style="font-weight: bold; padding-bottom: 20px;">ME PUEDES CONTACTAR AQUÍ:</h4>
		<div class="row">
                    <div class="col-md-6" style="text-align: center">
			<!--<p style="font-size: 1.3em; font-weight: bold; text-align: left;"><?= $model->email?></p>-->
                        <i class="fas fa-envelope fa-4x" style="color: #337AB7"></i><p style="font-size: 20px;"><?= $model->email?></p>
                        <i class="fas fa-phone-square fa-4x" style="color: #337AB7"></i><p style="font-size: 20px"><?= $model->telefono?></p>
                    </div>
                    <div class="col-md-6" style="text-align: center">
                        <a href="<?= $linkedin ?>" target="_blank"><i class="fab fa-linkedin fa-4x"></i></a><span><p style="font-size: 20px;">Linkedin</p>
                            <a href="<?= $model->web ?>" target="_blank"><i class="fab fa-chrome fa-4x"></i></a><p style="font-size: 20px;">Visita mi Web</p>
                    </div>        
             
                </div>
        </div>        


  


