<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Identifícate';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login" style="padding: 100px 200px">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Complete los siguientes campos para iniciar sesión:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-4\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-0 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true,'placeholder' => 'Usuario'])->label('') ?>

        <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Contraseña'])->label('') ?>


         
            
        		<?= $form->field($model, 'rememberMe')->checkbox([
            		'template' => "<div class=\"col-lg-2\">{input} {label}</div>\n<div class=\"col-lg-3\">{error}</div>",
        		])->label('Recuerdame') ?>

        
    
                                                                                              
        <div class="form-group">
            <div class="col-lg-4">
                <?= Html::submitButton('Entrar', ['class' => 'btn btn-primary btn-lg btn-block', 'name' => 'login-button']) ?>
            </div>
          
        </div>
 		 <div class="col-lg-4" style="text-align:right;margin-top:-10px;">
           	<a href="http://talentoslanzadera.werobsolutions.com/web/index.php/site/recoverpass">¿Has olvidado tu contraseña?</a>
  	    </div> 
  

    <?php ActiveForm::end(); ?>

   
</div>
