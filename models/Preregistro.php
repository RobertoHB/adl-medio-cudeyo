<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "preregistro".
 *
 * @property int $id
 * @property string $nombre
 * @property string $apellidos
 * @property string $email
 */
class Preregistro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'preregistro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellidos', 'email'], 'required'],
            [['nombre'], 'string', 'max' => 100],
            [['apellidos', 'email'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'email' => 'Email',
        ];
    }
     public function existeSolicitud($email)
    { 
        
       $resultado = Preregistro::find()->where(['email' => $email])->one();
       
        if(!empty($resultado)){
            return true;
        }else{
            return false;
       }
           
    }
    
    public function guardarSolicitud($nombre,$apellidos,$email){
        $nueva_solicitud = new Preregistro();
        $nueva_solicitud->nombre = $nombre;
        $nueva_solicitud->apellidos = $apellidos;
        $nueva_solicitud->email = $email;
        $nueva_solicitud->save();
    }
}
