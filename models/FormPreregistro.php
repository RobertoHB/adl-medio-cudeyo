<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class FormPreregistro extends Model
{
    public $fechalanzadera;
    public $nombre;
    public $apellidos;
    public $movil;
    public $email;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['nombre','apellidos','email','movil','fechalanzadera'], 'required'],
            [['nombre','apellidos','email','fechalanzadera'], 'string'],
             [['movil'], 'integer'],
          
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            
        ];
    }


}
