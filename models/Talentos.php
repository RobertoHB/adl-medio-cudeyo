<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\Url;
use yii\imagine\Image;  
use Imagine\Image\Box;  
use Imagine\Gd;

/**
 * This is the model class for table "talentos".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $profesion
 * @property string|null $tipo
 * @property string|null $lanzadera
 * @property int|null $telefono
 * @property string|null $email
 * @property string|null $web
 * @property string|null $linkedin
 * @property string|null $foto
 * @property string|null $sobremi
 * @property string|null $carta
 */
class Talentos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $imageFile;
 
     
    public static function tableName()
    {
        return 'talentos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'telefono'], 'integer'],
            [['sobremi', 'carta'], 'string'],
            [['particular', 'empresa'], 'boolean'],
            [['nombre', 'profesion', 'tipo', 'lanzadera', 'foto'], 'string', 'max' => 300],
            [['apellidos', 'email', 'web', 'linkedin'], 'string', 'max' => 200],
            [['id'], 'unique'],
        ];
       return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }
    

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'profesion' => 'Profesion',
            'tipo' => 'Tipo',
            'particular' => 'Particular',
            'empresa' => 'Empresa',
            'lanzadera' => 'Lanzadera',
            'telefono' => 'Telefono',
            'email' => 'Email',
            'web' => 'Web',
            'linkedin' => 'Linkedin',
            'foto' => 'Foto',
            'sobremi' => 'Sobremi',
            'carta' => 'Carta',
        ];
    }
    
    public function upload()
    {
        if ($this->validate()) {
//        $this->imageFile->saveAs('../web/img/Medio Cudeyo/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
          $foto = $this->eliminar_acentos($this->apellidos.$this->nombre);          
          $this->imageFile->saveAs('../web/img/'. $this->lanzadera.'/'.$foto.'.png');
          	return true;
        } else {
          	return false;
        }
    }
  
  	//reemplazar acentos, tildes y caracteres rarosPHP
	public function eliminar_acentos($cadena){
		
		//Reemplazamos la A y a
		$cadena = str_replace(
		array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
		array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
		$cadena
		);

		//Reemplazamos la E y e
		$cadena = str_replace(
		array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
		array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
		$cadena );

		//Reemplazamos la I y i
		$cadena = str_replace(
		array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
		array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
		$cadena );

		//Reemplazamos la O y o
		$cadena = str_replace(
		array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
		array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
		$cadena );

		//Reemplazamos la U y u
		$cadena = str_replace(
		array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
		array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
		$cadena );

		//Reemplazamos la N, n, C y c
		$cadena = str_replace(
		array('Ñ', 'ñ', 'Ç', 'ç'),
		array('N', 'n', 'C', 'c'),
		$cadena
		);
		//Reemplazamos espacios en blanco
		$cadena = str_replace(' ', '', $cadena);
		
		return $cadena;
	}
  
	public function resize_image($new_path) {
      $image = imagecreatefrompng($new_path);
      //Now calculate the ratio:
      $ratio = 700 / imagesx($image); // 700 for the width you want... imagesx() to determine the current width
      //Get the scaled height:
      $height = imagesy($image) * $ratio; // imagesy() to determine the current height
	 //Do the actual resize:
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $image, 0, 0, 0, 0, $width, $height, imagesx($image), imagesy($image));
      $image = $new_image; // $image has now been replaced with the resized one.
      return $image;
	}


}


    
 

