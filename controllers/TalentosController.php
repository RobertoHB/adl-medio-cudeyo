<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use app\models\Talentos;
use app\models\TalentosSearch;
use app\models\Preregistro;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UploadForm;
use yii\web\UploadedFile;
use app\models\User;
use app\models\Users;
use yii\imagine\Image;
use Imagine\Gd;
use Imagine\Image\Box;  
use Imagine\Image\BoxInterface;
/**
 * AlumnosController implements the CRUD actions for Alumnos model.
 */
class TalentosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Alumnos models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest || User::isUserUsuario(Yii::$app->user->identity->getId())){
            return $this->redirect(['/site/index']);
        }  
        
        $searchModel = new TalentosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Alumnos model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if(Yii::$app->user->isGuest || User::isUserUsuario(Yii::$app->user->identity->getId())){
            return $this->redirect(['/site/index']);
        }  
        
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Alumnos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->isGuest || User::isUserUsuario(Yii::$app->user->identity->getId())){
            return $this->redirect(['/site/index']);
        }  
        
        $model = new Talentos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->upload()) {
                // el archivo se subió exitosamente
              	
              
            return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    
    }
        public function actionPerfil()
    {
        //if(Yii::$app->user->isGuest || !User::isUserUsuario(Yii::$app->user->identity->getId())){
            //return $this->redirect(['site/index']);
       //}  
       
         $model = new Talentos();    
           
        $email_logueado = Yii::$app->user->identity->getEmail();
       
        if ($model->load(Yii::$app->request->post())) {
           $accion = 'guardar';
          	$model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $model->upload();
          		
              
              if($this->findModelEmail($email_logueado,$accion)){
              
                 $registro_actualizar = new Talentos;
                  
                 $registro_actualizar = Talentos::find()->where(['id' =>$model->id])->one();
                 
                 $registro_actualizar->nombre = $model->nombre;
                 $registro_actualizar->apellidos = $model->apellidos;
                 $registro_actualizar->profesion = $model->profesion;
                 
            
                 $registro_actualizar->particular = $model->particular;
                 $registro_actualizar->empresa = $model->empresa;
               
                 $registro_actualizar->lanzadera = $model->lanzadera;
                 $registro_actualizar->telefono = $model->telefono;
                 $registro_actualizar->email = $model->email;
                 $registro_actualizar->web = $model->web;
                 $registro_actualizar->linkedin = $model->linkedin;
                 $registro_actualizar->sobremi = $model->sobremi;
                 $registro_actualizar->carta = $model->carta;
                
                 $registro_actualizar->imageFile = UploadedFile::getInstance($registro_actualizar, 'imageFile');
                    if(!empty($registro_actualizar->imageFile)){
   
                      $registro_actualizar->upload();
                     
                   	}      
                        
                $registro_actualizar->update();
                   
                $msg = "Registro Actualizado correctamente";
               
            }else{        
                
                  $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
                  if(!empty($model->imageFile)){
                      $model->upload();
                  
                  }      
                
                  $model->save();
                    
                  $msg = "Registro Guardado correctamente";
            }
           
            return $this->render('perfil', ['model' => $model,'msg' => $msg]);
         
        }else{
           $accion = "modelo";
           
            if($model = $this->findModelEmail($email_logueado,$accion)){
              
                return $this->render('perfil', [
                    'model' => $model,
                ]);
            }
   
        }   


    }

    public function actionDelete_perfil($id){
       
      //eliminamos la ficha del talento
       	$logo_ayto = Url::to('@web/img/lg_ayuntamiento.png', true);
        $logo_lanzadera = Url::to('@web/img/lg_lanzaderas3.png',true);
      
        $modelo_eliminar = new Talentos;          
        $modelo_eliminar = Talentos::find()->where(['id' =>$id])->one();
        if(!empty($modelo_eliminar)){
            $email_talento =  $modelo_eliminar->email; 
            $foto = $modelo_eliminar->eliminar_acentos($modelo_eliminar->apellidos.$modelo_eliminar->nombre);
            $ruta_foto_eliminar = "../web/img/".$modelo_eliminar->lanzadera."/". $foto.".png";
          
            $modelo_eliminar->delete();
            //$msg= "Perfil de Talento eliminado con éxito";
            //$model = new Talentos();
            //return $this->render('perfil', ['model' => $model,'msg' => $msg]);
        }
      
      //eliminamos el registro de presolicitud
        $preregistro_eliminar = new Preregistro;          
        $preregistro_eliminar = Preregistro::find()->where(['email' =>$email_talento])->one();
        if(!empty($preregistro_eliminar)){
            $preregistro_eliminar->delete();
        }
      
      // desactivamos el registro de login
        $deshabilitar_login = new Users;          
        $deshabilitar_login = Users::find()->where(['email' =>$email_talento])->one();
        if(!empty($deshabilitar_login)){
            $deshabilitar_login->activate = 0;
            $deshabilitar_login->update();
        }
      
      // eliminamos la foto del talento
       
       $filename = $ruta_foto_eliminar;
      if (file_exists($filename)) {
          $success = unlink($filename);

          if (!$success) {
               throw new Exception("No se puede eliminar $filename");
          }
      }
      
       $html = '
                <html lang="en">
                <head>
                        <meta charset="UTF-8">
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
                        <title>Nuevo PreregistroTalento Lanzadera</title>
                        <style type="text/css">


                        </style>
                </head>
                <body style="max-width:1000px;">    

                <div width="100%" align="center">
                        <img src="'.$logo_ayto.'" width="150px" border="0" alt="" />
                        <label style="padding:40px 100px 40px 100px; font-weight: bold;font-size:30px;">LANZADERA TALENTOS</label>
                        <img src="'.$logo_lanzadera.'" width="120px" border="0" alt=""  />
                       
                </div>

                <div width="100%" align="center">
                        <div style="text-align:center;color:#c62828;font-weight: bold;font-size:30px;">
                            <p style="">¡Hasta Pronto! </p>
                        </div>
                        <div style="text-align:center;font-size:20px">
                            <p>Te has dado de baja con éxito de la Web de la Lanzadera de<p/> 
                            <p>Empleo.</p>
                        </div>
                       
                </div>

                <div width="100%" align="center" style="margin-top:50px">
                        <a style="color:white;background-color:#368BD6;text-align: center; font-size: 22px; font-family: arial; font-weight: bold; padding: 20px 20px 20px 20px;border-radius:5px;text-decoration:none;" href="http://talentoslanzadera.werobsolutions.com/web/index.php/site/index">Vuelve Cuando Quieras</a>                                       
                        <p style="padding-top:15px;">(este es un enlace a la página principal de la Web)</p>

                </div>


                </body>
                </html>';
          
         
               
               $contenido = Yii::$app->mailer->compose();
           
              // $contenido->attach($path.'/'.'lg_ayuntamiento.png');
                //->attach($path.'/'.'nif.jpg')
                //->attach($path.'/'.'firma.png')    
                //->attachContent('Contenido adjunto', ['fileName' => 'attach.txt', 'contentType' => 'text/plain'])
                $contenido->setTo([$email_talento]);
                $contenido->setFrom([Yii::$app->params['senderEmail']]);
                $contenido->setReplyTo([Yii::$app->params['senderEmail']]);
                $contenido->setSubject('Baja Talento Lanzadera');
                $contenido->setTextBody('');
                $contenido->setHtmlBody($html);
                $contenido->send();
      
      			return $this->redirect(["/site/login"]);
        
        
    }
    /**
     * Updates an existing Alumnos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
       
        $model = $this->findModel($id);
     
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
             $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
             if(!empty($model->imageFile)){
                if ($model->upload()) {
                // el archivo se subió exitosamente
                    return $this->redirect(['update', 'id' => $model->id]);
                }
            }
            
            return $this->redirect(['update', 'id' => $model->id]);
      
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Alumnos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Alumnos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Alumnos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Talentos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function findModelEmail($email,$accion)
    { 
        
       $form_vacio = new Talentos();
       $resultado = Talentos::find()->where(['email' => $email])->one();
    
       if(!empty($resultado)){
           
            if($accion == "modelo") {
                return $resultado;
                
            }else{
                return true;
            }    
            
       }else{
           
            if($accion == "guardar") {
                return false;
            }else{
                return $form_vacio;
            }    
       }
           
           

        
        
   

        throw new NotFoundHttpException('The requested page does not exist.');
    }


//        var_dump($_REQUEST);
//        exit;
//     if(isset($_FILES['archivo'])){
//        $ruta_origen = '../web/img/';
//        $carpeta = '../web/img/'.$lanzadera.'/';
//       
//        $fichero_origen ='../web/img/'.$lanzadera.'/'.$id.'/'.'jpg';
//        
//        if(!file_exists($carpeta))
//            $ruta_talento = mkdir($ruta_origen.$lanzadera,true);
// 
//         //$fichero_subido = $ruta_foto . basename($_FILES['archivo']['name']);
//        
//        if (file_exists($fichero_origen)) {
//            unlink($fichero_origen);
//        }
//        if (move_uploaded_file($_FILES['archivo']['tmp_name'], $fichero_origen)) {
//           return $this->redirect(Yii::$app->request->referrer);
//        } else {
//            echo "¡Posible ataque de subida de ficheros!\n";
//            return $this->redirect(Yii::$app->request->referrer);
//        }
//
//          
//     }else{
//         echo "no llega el fichero";
//     }
        
        
   
}