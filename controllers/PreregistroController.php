<?php

namespace app\controllers;
use app\models\Preregistro;
use app\models\FormRegister;

class PreregistroController extends Controller
{
    public function actionIndex()
    {
              if(Yii::$app->user->isGuest || !User::isUserAdmin(Yii::$app->user->identity->getId())){
            return $this->redirect(['preregistro/index']);
        }  
        
        $searchModel = new PreregistroSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
   

}
