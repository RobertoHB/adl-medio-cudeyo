<?php
namespace app\controllers;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\ContactForm;
use app\models\LoginForm;
use app\models\FormRegister;
use app\models\Users;
use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use app\models\Talentos;
use app\models\Filtro;
use app\models\FormPreregistro;
use app\models\Preregistro;
use yii\swiftmailer;
use yii\web\Session;
use app\models\FormRecoverPass;
use app\models\FormResetPass;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
                 
        ];       
    
    }

    
    
    public function actionUsuario(){
        return $this->render("/talentos/create");
    }
    public function actionAdministrador(){
        return $this->render("/talentos/index");       
    }
     
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
 
    

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
        $modelo = new Filtro();
        
         
         if ($modelo->load(Yii::$app->request->post())){
             
          $texto_buscar_genero = substr($modelo->textoBuscar, 0, -6); 
        
             
            $query = Talentos::find()
            ->orWhere(['particular' => $modelo->particular])
            ->orWhere(['empresa' => $modelo->empresa])
            ->andFilterWhere(['like', 'profesion', $texto_buscar_genero])
            //->andFilterWhere(['like', 'sobremi', $texto_buscar_genero])
            //->orFilterWhere(['like', 'carta', $texto_buscar_genero])
            ->orderBy(['apellidos' => SORT_DESC]);
            
         }else{
            $query = Talentos::find()
            ->orderBy(['apellidos' => SORT_DESC]);
              
         }
         
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
            return $this->render('index',['dataProvider' => $dataProvider,'modelo'=>$modelo]);
    }   

    /**
     * Login action.
     *
     * @return Response|string
     */
 public function actionLogin()
    {
     
        //if (!Yii::$app->user->isGuest) {
         //   return $this->goHome();
        //}
      
        $model = new LoginForm();
       
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if(User::isUserAdmin(Yii::$app->user->identity->id)){
                return $this->redirect(["/talentos"]);
            }else{
                return $this->redirect(["/talentos/perfil"]);
            }
        }else{
            $model->password = '';
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
       
       return $this->goHome();
    }

    public function actionMostrartalento($id)
    {
      
        $query = Talentos::find()
             ->andWhere(['id' => $id]);
          
       
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            
        ]);
        return $this->render('perfil',['dataProvider' => $dataProvider]);
    
      
        
    }            
     public function actionQuesonlanzaderas()
    {
      
        return $this->render('queson_lanzaderas');
 
    }            
        public function actionAutorizacion()
    {
      
        return $this->render('autorizacion');
 
    } 
      
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
    
 
     private function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }
  
 public function actionConfirm()
 {
    $table = new Users;
    if (Yii::$app->request->get())
    {
   
        //Obtenemos el valor de los parámetros get
        $id = Html::encode($_GET["id"]);
        $authKey = $_GET["authKey"];
      	$contra = $_GET["contra"];
    
        if ((int) $id)
        {
            //Realizamos la consulta para obtener el registro
            $model = $table
            ->find()
            ->where("id=:id", [":id" => $id])
            ->andWhere("authKey=:authKey", [":authKey" => $authKey]);
 
            //Si el registro existe
            if ($model->count() == 1)
            {
                $activar = Users::findOne($id);
                $activar->activate = 1;
                if ($activar->update())
                {
                  //se envia el correo al usuario con los datos de logeo y se devuelve la vista al register con el mensaje correspondiente
                  return $this->redirect(array('site/envio_credenciales', 'usuario' => $activar->username, 'contra' => $contra));
                    //echo "Enhorabuena registro llevado a cabo correctamente, redireccionando ...";
                    //echo "<meta http-equiv='refresh' content='8; ".Url::toRoute("/web/index.php")."'>";
                  
                }
                else
                {
                    echo "Ha ocurrido un error al realizar el registro, redireccionando ...";
                    echo "<meta http-equiv='refresh' content='8; ".Url::toRoute("site/login")."'>";
                }
             }
            else //Si no existe redireccionamos a login
            {
                return $this->redirect(["site/login"]);
            }
        }
        else //Si id no es un número entero redireccionamos a login
        {
            return $this->redirect(["site/login"]);
        }
    }
 }
 

    /**
     * Displays about page.
     *
     * @return string
     */
 public function actionRegister(){
   
    if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
 

  //Creamos la instancia con el model de validación
  $model = new FormRegister();
 
  
  //Mostrará un mensaje en la vista cuando el usuario se haya registrado
  $msg = null;
  	   
  if ($model->load(Yii::$app->request->post())){
     
   if($model->validate()){
    //Preparamos la consulta para guardar el usuario
   	$table = new Users;
   	$table->username = $model->username;
    $table->email = $model->email;
    $table->tipo = "invitado";
    //Encriptamos el password
    $table->password = crypt($model->password, Yii::$app->params["salt"]);
    //Creamos una cookie para autenticar al usuario cuando decida recordar la sesión, esta misma
    //clave será utilizada para activar el usuario
    $table->authKey = $this->randKey("abcdef0123456789", 200);
    //Creamos un token de acceso único para el usuario
    $table->accessToken = $this->randKey("abcdef0123456789", 200);
     
        //Si el registro es guardado correctamente
        if ($table->insert()){
         //Nueva consulta para obtener el id del usuario
         //Para confirmar al usuario se requiere su id y su authKey
         $user = $table->find()->where(["email" => $model->email])->one();
         $id = urlencode($user->id);
         $authKey = urlencode($user->authKey);

         //$subject = "Confirmar registro";
         //$body = "<h1>Haga click en el siguiente enlace para finalizar tu registro</h1>";
         //$body .= "<a href='http://talentoslanzadera.werobsolutions.com/web/index.php?r=site/confirm&id=".$id."&authKey=".$authKey."'>Confirmar</a>";

         //Enviamos el correo
         //Yii::$app->mailer->compose()
         //->setTo($user->email)
         //->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
         //->setSubject($subject)
         //->setHtmlBody($body)
         //->send();

         //$model->username = null;
         //$model->email = null;
         //$model->password = null;
         //$model->password_repeat = null;
          return $this->redirect(array('site/confirm', 'id' => $id, 'authKey' => $authKey, 'contra' => $model->password));
          //$msg = "Enhorabuena, ahora sólo falta que confirmes tu registro en tu cuenta de correo";
        }else{
        $msg = "Ha ocurrido un error al llevar a cabo tu registro";
        }
     
   }else{
    $model->getErrors();
   }
    
  }
	return $this->render("register", ["model" => $model, "msg" => $msg]);
 }
  public function actionPreregistro()
    {
    

        //$message = new \Swift_Message();
     	//$image_ayto = \Swift_Image::fromPath(Yii::getAlias("@webroot/img/lg_ayuntamiento.png"));
  		//$image_lanza = \Swift_Image::fromPath(Yii::getAlias("@webroot/img/lg_lanzaderas3.png"));    

        //$image_ayto = Swift_Image::fromPath(dirname(Yii::app()->getBasePath()) . '/img/lg_ayuntamiento.png');
        //$image_lanza = Swift_Image::fromPath(dirname(Yii::app()->getBasePath()) . '/img/lg_lanzaderas3.png');
        //$logo_ayto = $message->embed($image_ayto); 
        //$logo_lanza = $message->embed($image_lanza); 
    
        $mensaje="";
        //$path = "http://localhost/lanzadera_talentos/web/img/";

        $logo_ayto = Url::to('@web/img/lg_ayuntamiento.png', true);
        $logo_lanza = Url::to('@web/img/lg_lanzaderas3.png',true);
         
        //$type_log_ayto = pathinfo($logo_ayto, PATHINFO_EXTENSION);
        //$type_log_lanzadera = pathinfo($logo_lanzadera, PATHINFO_EXTENSION);
 
        // Cargando la imagen
        //$data_log_ayto = file_get_contents($logo_ayto);
        //$data_log_lanzaderas = file_get_contents($logo_lanzadera);

        // Decodificando la imagen en base64
        //$logo_ayto_base64 = 'data:image/' . $type_log_ayto . ';base64,' . base64_encode($data_log_ayto);
        //$logo_lanzaderas_base64 = 'data:image/' . $type_log_lanzadera . ';base64,' . base64_encode($data_log_lanzaderas);

  
        $model = new FormPreregistro(); 
        if ($model->load(Yii::$app->request->post())) {
            
            //confirmamos que la solicitud no existe
            $existe_solicitud = new Preregistro();
            
            if ($existe_solicitud->existeSolicitud($model->email)){
                $mensaje = "Solicitud de registro ya enviada con anterioridad";
                return $this->render('form_autorizo',['model' => $model,'msg'=>$mensaje]);
            }
          
          
         		 $contenido = Yii::$app->mailer->compose('@app/mail/layouts/correo_solicitud', [
                      'fechalanzadera' => $model->fechalanzadera,
                  	  'nombre' => $model->nombre,
                      'apellidos' => $model->apellidos,
                      'email' => $model->email,
                      'movil' => $model->movil,
                      'logo_ayto' => $logo_ayto,
                      'logo_lanza' => $logo_lanza
                 ]);
         
           
              // $contenido->attach($path.'/'.'lg_ayuntamiento.png');
                //->attach($path.'/'.'nif.jpg')
                //->attach($path.'/'.'firma.png')    
                //->attachContent('Contenido adjunto', ['fileName' => 'attach.txt', 'contentType' => 'text/plain'])
               $contenido->setTo([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']]);
               $contenido->setFrom([Yii::$app->params['senderEmail']]);
               $contenido->setReplyTo([Yii::$app->params['senderEmail']]);
               $contenido->setSubject('Nuevo Preregistro Talento');
               $contenido->setTextBody('');
               //$contenido->setTextBody(array('logo_ayto' => $logo_ayto,'logo_lanza' => $logo_lanza), 'text/html');
               //$contenido->setHtmlBody('');
               $contenido->send();
             $mensaje = "Tu petición ha sido solicitada. En breve recibiras un correo con tus datos de acceso a nuestra Web.";
            $existe_solicitud->guardarSolicitud($model->nombre, $model->apellidos, $model->email);
            } 
//         }
        return $this->render('form_autorizo',['model' => $model,'msg'=>$mensaje]);
     
        
    } 
  
   public function actionEnvio_credenciales($usuario,$contra){
   	
     $logo_ayto = Url::to('@web/img/lg_ayuntamiento.png', true);
     $logo_lanzadera = Url::to('@web/img/lg_lanzaderas3.png',true);
     $html = '
                <html lang="en">
                <head>
                        <meta charset="UTF-8">
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
                        <title>Envio de credencdiales</title>
                        <style type="text/css">


                        </style>
                </head>
                <body style="max-width:1200px;">    

                <header width="100%" align="center">
                        <img src="'.$logo_ayto.'" width="150px" border="0" alt="" />
                        <label style="padding:40px 100px 40px 100px; font-weight: bold;font-size:30px;">LANZADERA TALENTOS</label>
                        <img src="'.$logo_lanzadera.'" width="120px" border="0" alt=""  />
                       
                </header>

                <main width="100%" align="center">
                        <div style="text-align:center;color:#c62828;font-weight: bold;font-size:30px;">
                            <p style="">¡Gracias por elegir nuestra Web! </p>
                        </div>
                        <div style="text-align:center;font-size:20px">
                            <p>A partir de ahora puedes acceder a tu cuenta y modificar tus <p/> 
                            <p>datos cuando quieras.</p>
                        </div>
                        <div style="text-align:left;margin-left:400px;margin-top:30px;font-size:20px;font-weight: bold;">
                            <p>Tus datos de acceso son:  </p>
                        </div>
                        <div style="font-size:20px;text-align:left;margin-left:500px">
                          
                                <p>Usuario: <i style="font-weight: bold">'. $usuario.'</i></p>
                                <p>Contraseña: <i style="font-weight: bold">'.$contra.'</i></p>
                           
                        </div>    

                </main>

                <footer width="100%" align="center" style="margin-top:35px;">
                	<div style="text-align:center;font-size:20px">
                		<p style="padding-top:15px;"><label style="color:#8AE5C9">El primer paso</label> es terminar tu <label style="color:#8AE5C9">Registro</label> para que las empresas</p>
                        <p>puedan encontrarte.</p>
                    </div>    
                	<div style="text-align:center;margin-top:50px;">
                    	<a style="color:white;background-color:#368BD6;text-align: center; font-size: 22px; font-family: arial; font-weight: bold; padding: 20px 20px 20px 20px;border-radius:5px;text-decoration:none;" href="http://talentoslanzadera.werobsolutions.com/web/index.php/site/login">Completa Ahora tus Datos</a>                                  
                    	<p style="padding-top:15px;">(este es un enlace a la página de la Web donde puede autorizar el registro de este usuario)</p>
					</div>
                </footer>


                </body>
                </html>';
          
         	   $model = new FormRegister();
               $msg = "Registro realizado con éxito y enviadas las credenciales";
               $contenido = Yii::$app->mailer->compose();
           
              // $contenido->attach($path.'/'.'lg_ayuntamiento.png');
                //->attach($path.'/'.'nif.jpg')
                //->attach($path.'/'.'firma.png')    
                //->attachContent('Contenido adjunto', ['fileName' => 'attach.txt', 'contentType' => 'text/plain'])
                $contenido->setTo([$usuario]);
                $contenido->setFrom([Yii::$app->params['senderEmail']]);
                $contenido->setReplyTo([Yii::$app->params['senderEmail']]);
                $contenido->setSubject('Lanzadera empleo.Credenciales');
                $contenido->setTextBody('');
                $contenido->setHtmlBody($html);
                $contenido->send();
   
    		    return $this->render("register", ["model" => $model, "msg" => $msg]);
     
   }

  
  
  public function actionRecoverpass()
 {
    
    
  //Instancia para validar el formulario
   $model = new FormRecoverPass();
   

  //Mensaje que será mostrado al usuario en la vista
  $msg = null;
    
   //return $this->render("recoverpass", ["model" => $model, "msg" => $msg]);
    
  if ($model->load(Yii::$app->request->post()))
  {
   if ($model->validate())
   {
    //Buscar al usuario a través del email
    $table = Users::find()->where("email=:email", [":email" => $model->email]);
    
    //Si el usuario existe
    if ($table->count() == 1)
    {
     //Crear variables de sesión para limitar el tiempo de restablecido del password
     //hasta que el navegador se cierre
     $session = new Session;
     $session->open();
     
     //Esta clave aleatoria se cargará en un campo oculto del formulario de reseteado
     $session["recover"] = $this->randKey("abcdef0123456789", 200);
     $recover = $session["recover"];
     
     //También almacenaremos el id del usuario en una variable de sesión
     //El id del usuario es requerido para generar la consulta a la tabla users y 
     //restablecer el password del usuario
     $table = Users::find()->where("email=:email", [":email" => $model->email])->one();
     $session["id_recover"] = $table->id;
     
     //Esta variable contiene un número hexadecimal que será enviado en el correo al usuario 
     //para que lo introduzca en un campo del formulario de reseteado
     //Es guardada en el registro correspondiente de la tabla users
     $verification_code = $this->randKey("abcdef0123456789", 8);
     //Columna verification_code
     $table->verification_code = $verification_code;
     //Guardamos los cambios en la tabla users
     $table->save();
     
     //Creamos el mensaje que será enviado a la cuenta de correo del usuario
     $logo_ayto = Url::to('@web/img/lg_ayuntamiento.png', true);
     $logo_lanzadera = Url::to('@web/img/lg_lanzaderas3.png',true);
     $subject = "Recuperar Contraseña";
     $body =  '<header width="100%" align="center">
                        <img src="'.$logo_ayto.'" width="150px" border="0" alt="" />
                        <label style="padding:40px 100px 40px 100px; font-weight: bold;font-size:30px;">LANZADERA TALENTOS</label>
                        <img src="'.$logo_lanzadera.'" width="120px" border="0" alt=""  />                  
                </header>';
     $body .= '<p style="margin-top:40px;font-size:20px;">Pasos para recuperar su contraseña';
     $body .= '<p style="font-size:18px;">Copie el siguiente código de verificación para restablecer su contraseña ... ';
     $body .= '<strong>'.$verification_code.'</strong></p>';
     $body .= '<p style="font-size:18px;"><a href="http://talentoslanzadera.werobsolutions.com/web/index.php/site/resetpass">Recuperar contraseña</a></p>';
    

     //Enviamos el correo
     Yii::$app->mailer->compose()
     ->setTo($model->email)
     ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
     ->setSubject($subject)
     ->setHtmlBody($body)
     ->send();
     
     //Vaciar el campo del formulario
     $model->email = null;
     
     //Mostrar el mensaje al usuario
     $msg = "Le hemos enviado un mensaje a su cuenta de correo para que pueda resetear su contraseña";
    }
    else //El usuario no existe
    {
     $msg = "Ha ocurrido un error";
    }
   }
   else
   {
    $model->getErrors();
   }
  }
  return $this->render("recoverpass", ["model" => $model, "msg" => $msg]);
 }

 public function actionResetpass()
 {
  //Instancia para validar el formulario
    //return ("hola");
  $model = new FormResetPass;
 
  //Mensaje que será mostrado al usuario
  $msg = null;
  
  //Abrimos la sesión
  $session = new Session;
  $session->open();
  
  //Si no existen las variables de sesión requeridas lo expulsamos a la página de inicio
  if (empty($session["recover"]) || empty($session["id_recover"]))
  {
   return $this->redirect(["site/index"]);
  }
  else
  {
   
   $recover = $session["recover"];
   //El valor de esta variable de sesión la cargamos en el campo recover del formulario
   $model->recover = $recover;
   
   //Esta variable contiene el id del usuario que solicitó restablecer el password
   //La utilizaremos para realizar la consulta a la tabla users
   $id_recover = $session["id_recover"];
   
  }
  
  //Si el formulario es enviado para resetear el password
  if ($model->load(Yii::$app->request->post()))
  {
   if ($model->validate())
   {
    //Si el valor de la variable de sesión recover es correcta
    if ($recover == $model->recover)
    {
     //Preparamos la consulta para resetear el password, requerimos el email, el id 
     //del usuario que fue guardado en una variable de session y el código de verificación
     //que fue enviado en el correo al usuario y que fue guardado en el registro
     $table = Users::findOne(["email" => $model->email, "id" => $id_recover, "verification_code" => $model->verification_code]);
     
     //Encriptar el password
     $table->password = crypt($model->password, Yii::$app->params["salt"]);
     
     //Si la actualización se lleva a cabo correctamente
     if ($table->save())
     {
      
      //Destruir las variables de sesión
      $session->destroy();
      
      //Vaciar los campos del formulario
      $model->email = null;
      $model->password = null;
      $model->password_repeat = null;
      $model->recover = null;
      $model->verification_code = null;
      
      $msg = "Enhorabuena, contraseña reseteada correctamente, redireccionando a la página de login ...";
      $msg .= "<meta http-equiv='refresh' content='5; ".Url::toRoute("site/login")."'>";
     }
     else
     {
      $msg = "Ha ocurrido un error";
     }
     
    }
    else
    {
     $model->getErrors();
    }
   }
  }
  
  return $this->render("resetpass", ["model" => $model, "msg" => $msg]);
  
 }
   

}