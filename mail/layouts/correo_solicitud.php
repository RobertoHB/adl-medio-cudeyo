<?php
use yii\helpers\Html;
use yii\swiftmailer\Message;


$mensaje = new Message();
 		$type_log_ayto = pathinfo($logo_ayto, PATHINFO_EXTENSION);
        $type_log_lanzadera = pathinfo($logo_lanza, PATHINFO_EXTENSION);
 
        // Cargando la imagen
        $data_log_ayto = file_get_contents($logo_ayto);
        $data_log_lanzaderas = file_get_contents($logo_lanza);

        // Decodificando la imagen en base64
        $logo_ayto_base64 = 'data:image/' . $type_log_ayto . ';base64,' . base64_encode($data_log_ayto);
        $logo_lanzaderas_base64 = 'data:image/' . $type_log_lanzadera . ';base64,' . base64_encode($data_log_lanzaderas);

		$logo_1= base64_encode(file_get_contents($logo_ayto));
		$logo_2= base64_encode(file_get_contents($logo_lanza));

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">  		
           
                <head>
                        <meta charset="UTF-8">
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
                        <title>Nuevo PreregistroTalento Lanzadera</title>
                        <style type="text/css">


                        </style>
                </head>
                <body style="max-width:1200px;">    

                <header width="100%" align="center">
                        <img src="<?= $logo_ayto ?>" width="150px" border="0" alt="no image" />
                        <label style="padding:40px 100px 40px 100px; font-weight: bold;font-size:30px;">LANZADERA TALENTOS</label>
                        <img src="<?= $logo_lanza ?>" width="120px" border="0" alt="no image"  />
                       
                </header>

                <main width="100%" align="center">
                        <div style="text-align:center;color:#c62828;font-weight: bold;font-size:30px;">
                            <p style="">¡Nuevo Talento! </p>
                        </div>
                        <div style="text-align:center;font-size:20px">
                            <p>Un nuevo usuario quiere darse de alta en nuestra Web,<p/> 
                            <p>sólo tienes que autorizarlo.</p>
                        </div>
                        <div style="text-align:left;margin-left:400px;margin-top:30px;font-size:20px;font-weight: bold;">
                            <p>Sus datos son:  </p>
                        </div>
                        <div style="font-size:20px;text-align:left;margin-left:500px">
                          
                                <p>Año de su Lanzadera: <i style="font-weight: bold"><?= $fechalanzadera ?></i></p>
                                <p>Nombre: <i style="font-weight: bold"><?= $nombre ?></i></p>
                                <p>Apellidos: <i style="font-weight: bold"><?= $apellidos ?></i></p>
                                <p>Email: <i style="color:blue;font-weight:bold"><?= $email ?></i></p>
                                <p>Teléfono: <i style="font-weight: bold"><?= $movil ?></i></p>
                           
                        </div>    

                </main>

                <footer width="100%" align="center" style="margin-top:35px;">
                        <a style="color:white;background-color:#368BD6;text-align: center; font-size: 22px; font-family: arial; font-weight: bold; padding: 20px 20px 20px 20px;border-radius:5px;text-decoration:none;" href="http://talentoslanzadera.werobsolutions.com/web/index.php/site/register">Autoriza el Registro</a>             
                                           
                        <p style="padding-top:15px;">(este es un enlace a la página de la Web donde puede autorizar el registro de este usuario)</p>

                </footer>


                </body>
                </html>;